import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { ExcelService } from './excel.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { Papa } from 'ngx-papaparse';
// import { TableExport } from 'tableexport';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgxDatatableModule
  ],
  providers: [ExcelService, Papa],
  bootstrap: [AppComponent]
})
export class AppModule { }
