import { Component } from '@angular/core';
import { ExcelService } from './excel.service';
import { Papa } from 'ngx-papaparse';
declare var require: any;
const data: any = require('../app/company.json');
// import { TableExport } from 'tableexport';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  csvData: any;
  tableShow = false;
  isTable = false;
  csvKeys;
  checkedValue;
  testFinal;
  rows = [];
  loadingIndicator = true;
  reorderable = true;
  private columns: any;
  staticKeys = [{
    name: 'First Name',
    key: 'fname',
  }, {
    name: 'Last Name',
    key: 'last_name',
  }, {
    name: 'Third Name',
    key: 'third_name',
  }, {
    name: 'Forth Name',
    key: 'forth_name',
  }];
  finalCsvData;


  constructor(private papa: Papa, private excelService: ExcelService) {
    this.rows = data;
    setTimeout(() => { this.loadingIndicator = false; }, 1500);
  }

  fileChanged(files: FileList) {
    const options = {
      complete: (results, file) => {
        this.csvData = results.data;
        // tslint:disable-next-line:prefer-const
        let keys = this.csvData.shift(),
          i = 0, k = 0,
          obj = null,
          // tslint:disable-next-line:prefer-const
          output = [];
        this.csvKeys = keys;
        this.columns = this.csvKeys.map((value, i, arr) => {
          return {
            name: this.ucfirst(value).split(' ').join(' '),
            prop: value
          };
        });
        for (i = 0; i < this.csvData.length; i++) {
          obj = {};
          for (k = 0; k < keys.length; k++) {
            obj[keys[k]] = this.csvData[i][k];
          }
          output.push(obj);
        }
        this.csvData = output;
        this.finalCsvData = Object.assign({}, this.csvData);

        this.testFinal = this.csvKeys.map((item, index) => {

          // tslint:disable-next-line:no-shadowed-variable
          // this.staticKeys.forEach((i) => {
          //   if (i.key === item) {
          //     return {
          //       orignal: item,
          //       mapping: i.name
          //     };
          //   } else {
          return {
            orignal: item,
            mapping: ''
          };
          // }
        });


      // });
    console.log(this.testFinal);
    debugger

    // console.log(this.testFinal);
    // for (const csvKey of this.csvKeys) {
    //   for (const staticKey of this.staticKeys) {
    //     if (staticKey.key === csvKey) {
    //       this.checkedValue = staticKey.key;
    //       console.log('Same Key Value Found');
    //     } else {
    //       this.checkedValue = '';
    //       console.log('Same Key Value Not Found');
    //     }
    //   }
    // }
    console.log(this.finalCsvData);
    this.isTable = true;
  }
};
if (files && files.length > 0) {
  const file: File = files.item(0);
  const reader: FileReader = new FileReader();
  reader.readAsText(file);
  reader.onload = (e) => {
    const csv: any = reader.result; // temp ! added any
    this.papa.parse(csv, options);
  };
}
  }

ucfirst(str) {
  const firstLetter = str.substr(0, 1);
  return firstLetter.toUpperCase() + str.substr(1);
}

exportAsXLSX(): void {
  // this.tableFinal;
  // this.csvData;
  // debugger
}

onDetailToggle() {
  console.log('Detail Toggled', event);
}

headerChange(ev, i) {
  /* this.csvKeys;
  for (const key of this.csvKeys) {
    if (key === ev) {
      this.finalCsvData.map((Item, index) => {
        return {

        }
      });
    }
  }
  debugger */
  this.testFinal = this.csvKeys.map((item, index) => {
    return {
      orignal: item,
      mapping: ev
    };
  });
  debugger
  /*   const u = this.csvKeys.indexOf(ev);
    console.log(u);
    this.csvData.map((item, index) => {
      if (u === index) {
        console.log(item);
      }
    });
    console.log(this.csvKeys, '  ', this.csvData); */
}

}
